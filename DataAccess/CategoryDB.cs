﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Ch23ShoppingCartWCF
{
    public class CategoryDB { 
    
        public CategoryDB() { }

        public List<Category> GetCategories()
        {
            List<Category> list = new List<Category>();
            string sql = "SELECT CategoryID, ShortName, LongName "
                + "FROM Categories "
                + "ORDER BY ShortName";

            using (SqlConnection con = new SqlConnection(GetConnectionString())) {
                using (SqlCommand cmd = new SqlCommand(sql, con)) {
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (dr.Read()) 
                    {
                        Category c = new Category();
                        c.CategoryID = dr["CategoryID"].ToString();
                        c.ShortName = dr["ShortName"].ToString();
                        c.LongName = dr["LongName"].ToString();
                        list.Add(c);
                    }
                    dr.Close();
                }
            }
            return list;
        }

        public Category GetCategoryById(string id)
        {
            Category c = null;
            string sql = "SELECT CategoryID, ShortName, LongName "
                + "FROM Categories "
                + "WHERE CategoryID = @CategoryID";

            using (SqlConnection con = new SqlConnection(GetConnectionString())) {
                using (SqlCommand cmd = new SqlCommand(sql, con)) {
                    cmd.Parameters.AddWithValue("CategoryID", id);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dr.Read()) 
                    {
                        c = new Category();
                        c.CategoryID = dr["CategoryID"].ToString();
                        c.ShortName = dr["ShortName"].ToString();
                        c.LongName = dr["LongName"].ToString();
                    }
                    dr.Close();
                }
            }
            return c;
        }

        public int InsertCategory(Category c)
        {
            int affected;
            string sql = "INSERT INTO Categories "
                + "(CategoryID, ShortName, LongName) "
                + "VALUES (@CategoryID, @ShortName, @LongName)";

            using (SqlConnection con = new SqlConnection(GetConnectionString())) {
                using (SqlCommand cmd = new SqlCommand(sql, con)) {
                    cmd.Parameters.AddWithValue("CategoryID", c.CategoryID);
                    cmd.Parameters.AddWithValue("ShortName", c.ShortName);
                    cmd.Parameters.AddWithValue("LongName", c.LongName);
                    con.Open();
                    affected = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return affected;
        }

        public int UpdateCategory(Category c)
        {
            int affected;
            string sql = "UPDATE Categories "
                + "SET ShortName = @ShortName, LongName = @LongName "
                + "WHERE CategoryID = @CategoryID";

            using (SqlConnection con = new SqlConnection(GetConnectionString())) {
                using (SqlCommand cmd = new SqlCommand(sql, con)) {
                    cmd.Parameters.AddWithValue("CategoryID", c.CategoryID);
                    cmd.Parameters.AddWithValue("ShortName", c.ShortName);
                    cmd.Parameters.AddWithValue("LongName", c.LongName);
                    con.Open();
                    affected = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return affected;
        }

        public int DeleteCategory(Category c)
        {
            int affected;
            string sql = "DELETE FROM Categories "
                + "WHERE CategoryID = @CategoryID";

            using (SqlConnection con = new SqlConnection(GetConnectionString())) {
                using (SqlCommand cmd = new SqlCommand(sql, con)) {
                    cmd.Parameters.AddWithValue("CategoryID", c.CategoryID);
                    con.Open();
                    affected = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return affected;
        }

        private string GetConnectionString()
        {
            return ConfigurationManager
                .ConnectionStrings["Halloween"].ConnectionString;
        }
    }
}